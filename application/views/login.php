<?php include_once('includes/header.php'); ?>

<div class="container">
	<div class="card card-container">
		
		<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
		<p id="profile-name" class="profile-name-card"></p>

		<?php 

			echo form_open('main/login_validation' , 'class="form-signin"'); 

			echo "<span id='reauth-username' class='reauth-username'></span>";

			echo form_input('username" id="inputUsername" class="form-control" placeholder="Username" required autofocus');

			echo form_password('password" id="inputPassword" class="form-control" placeholder="Password" required');

			echo form_submit('login_submit" type="submit" class="btn btn-lg btn-primary btn-block btn-signin"', ' Sign in'); 

			echo form_close(); 

		?> 
		<br><a href='<?php echo base_url()."index.php/main/register"; ?>'><center>Create new account</center></a>
 		<br>
 		<?php if (validation_errors()) : ?> 
 			<?= validation_errors() ?> 
 		<?php endif; ?>
 		<?php if (isset($error)) : ?> 
 			<?= $error  ?> 
 		<?php endif; ?> 

 		
	</div> 

	
</div> 

<?php include_once('includes/footer.php'); ?>