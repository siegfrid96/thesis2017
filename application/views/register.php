<?php include_once('includes/header.php'); ?>

<div class="container">
	<div class="card card-container">
		
		<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
		<p id="profile-name" class="profile-name-card"></p>

		<?php 

			echo form_open('main/register_validation' , 'class="form-signin"'); 

			echo "<span id='reauth-username' class='reauth-username'></span>";

			echo form_input('username',null, 'id="inputUsername" class="form-control" placeholder="Username" required autofocus');

			echo form_password('password',null, ' id="inputPassword" class="form-control" placeholder="Password" required');

			echo form_password('cpassword',null, ' id="inputPassword" class="form-control" placeholder="Confirm Password" required');

			echo form_input('email',null, ' id="inputUsername" class="form-control" placeholder="Email" required');

			echo form_submit('login_submit" type="submit" class="btn btn-lg btn-primary btn-block btn-signin"', ' Sign up'); 

			echo form_close(); 

		?>

 		<?php echo validation_errors(); ?><br>
 		<a href='<?php echo base_url()."index.php/main/login"; ?>'><center>Back to Login</center></a>
	</div> 

	
</div> 

<?php include_once('includes/footer.php'); ?>