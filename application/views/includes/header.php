<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title> <?php echo $title; ?> </title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login.css?<?php echo time(); ?>">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script
	src="https://code.jquery.com/jquery-3.1.1.min.js"
	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/login.js?<?php echo time(); ?>"></script>
</head> 

<body>

<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> 
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1-1</a></li>
            <li><a href="#">Page 1-2</a></li>
            <li><a href="#">Page 1-3</a></li>
          </ul>
        </li>
        <li><a href="#">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">

      	<!-- If Else Sign up/ View profile -->
      	<?php if($this->session->userdata('is_logged_in')): ?>
        	<li><a href="#"><span class="glyphicon glyphicon-user"></span> My Profile</a></li>
        <?php else: ?>
        	<li><a href="<?php echo base_url() . "index.php/main/register" ?>"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <?php endif; ?>
        
        <!-- If Else Login/Logout -->
        <?php if($this->session->userdata('is_logged_in')): ?>
        	<li><a href="<?php echo base_url() . "index.php/main/logout" ?>"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        <?php else: ?>
        	<li><a href="<?php echo base_url() . "index.php/main/login" ?>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> 
        <?php endif; ?>

      </ul>
    </div>
  </div>
</nav>