<?php

class Model_users extends CI_Model {

	public function can_login($username, $password){

		$this->db->select('password');
		$this->db->from('accounts');
		$this->db->where('username', $username);
		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password, $hash);
	}

	public function add_temp_user($key){

		$data = array( 
				'email' => $this->input->post('email'),
				'username' => $this->input->post('username'),
				'password' => $this->hash_password($this->input->post('password')),
				'secretkey' => $key
			);

		$query = $this->db->insert('temp_users', $data);

		if($query){
			return true;
		} else {
			return false;
		} 
	}

	public function add_user($key){
		$this->db->where('secretkey', $key);
		$temp_user = $this->db->get('temp_users');

		if ($temp_user){
			$row = $temp_user->row();

			$data = array(
					'email' => $row->email,
					'username' => $row->username,
					'password' => $row->password
				);

			$is_userCreated = $this->db->insert('accounts', $data);
		}

		if ($is_userCreated){
			$this->db->where('secretkey', $key);
			$this->db->delete('temp_users'); 

			return $data['email'];
		} else return false;
	}

	public function is_validKey($key){
		$this->db->where('secretkey', $key);
		$query = $this->db->get('temp_users');

		if ($query->num_rows() == 1){
			return true;
		} else return false;
	}

	private function hash_password($password) {
		
		return password_hash($password, PASSWORD_BCRYPT);
		
	} 

	private function verify_password_hash($password, $hash) {
		
		return password_verify($password, $hash);
		
	}
}