<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		$this->load->helper('url'); 
	}

	public function index(){
		$this->login();
	}

	public function login(){ 
		if($this->session->userdata('is_logged_in')){	// if login then remove session
			$this->session->sess_destroy();
		}

		$data['title'] = "Login";
		$this->load->view('login', $data);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('main/login');
	}

	public function register(){
		if($this->session->userdata('is_logged_in')){
			$this->session->sess_destroy();
		}

		$data['title'] = "Register";
		$this->load->view('register', $data);
	}

	public function members(){
		$data['title'] = "Members";
		if ($this->session->userdata('is_logged_in')){
			$this->load->view('members',$data);	
		} else {
			redirect('main/restricted');
		}
		
	}

	public function restricted(){
		$this->load->view('errors/restricted');
	}

	function auto_version($file='')
	{
		if(!file_exists($file))
			return $file;
		
		$mtime = filemtime($file);
		return $file.'?'.$mtime;
	}

	public function login_validation(){

		$this->load->library('form_validation');
		$this->load->model('model_users');

		$this->form_validation->set_rules('username','Username','required|trim');
		$this->form_validation->set_rules('password','Password','required|trim');

		if($this->form_validation->run()){ 

			$username = $this->input->post('username'); 
			$password = $this->input->post('password');

			if ($this->model_users->can_login($username,$password)){
				$sessionData = array(
					'username' => $this->input->post('username'),
					'is_logged_in' => 1
				);
				$this->session->set_userdata($sessionData);
				redirect('main/members'); 
			} else { 
				$data = array(
					'title' => 'Login',
					'error' => 'Incorrect Credentials'
				);
				$this->load->view('login', $data); 
			}
 
		} else {
			$data['title'] = 'Login';
			$this->load->view('login', $data); 
		}
	}

	public function register_validation(){

		$this->load->library('form_validation');
		$this->load->model('model_users');

		$this->form_validation->set_rules('username','Username','required|trim|is_unique[accounts.username]');

		$this->form_validation->set_rules('password','Password','required|trim');

		$this->form_validation->set_rules('cpassword','Confirm Password','required|trim|matches[password]');

		$this->form_validation->set_rules('email','Email',
			'required|trim|valid_email|is_unique[accounts.email]');

		$this->form_validation->set_message('is_unique', " %s already Exists");

		if ($this->form_validation->run()){

			// Send Email Confirmation

			$key = md5(uniqid());

			$this->load->library('email', array('mailtype' => 'html'));

			$this->email->from('siegfrid@myweb.com',"siegfrid");
			$this->email->to($this->input->post('email'));
			$this->email->subject("Confirm your account.");

			$message = "<p>Thank you for signing up!</p>";
			$message .= "<p><a href='" .base_url(). "index.php/main/register_user/$key' > Click Here </a> 
						to confirm your account</p>";

			$this->email->message($message);

			//Send Email to user
			if($this->model_users->add_temp_user($key)){
				if ($this->email->send()){
					echo "The email has been sent!";
				} else echo "The email couldn't be sent"; 
			} else echo "problem adding to database";  

		} else {
			$this->load->view('register');
		}

	}

	public function register_user($key){
		$this->load->model('model_users');

		if ($this->model_users->is_validKey($key)){
			if ($newemail = $this->model_users->add_user($key)){
				$data = array(
						'email' => $newemail ,
						'is_logged_in' => 1
					);

				$this->session->set_userdata($data);
				redirect('main/members');

 			} else echo "cannot create user"; 
		} else echo "invalid key";
	}

	//not used
	public function validate_credentials(){
		$this->load->model('model_users');

		if ($this->model_users->can_login()){
			return true;
		} else {
			$this->form_validation->set_message('validate_credentials', 'Incorrect Credentials');
			return false;
		}
	}

}
